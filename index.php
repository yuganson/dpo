﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/index.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/totopbutton.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pushy.css">
    <link rel="shortcut icon" href="img/icon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
    
    <title>ДПО Политех - Главная</title>
</head>
<body>
    <header>
        <div class="header">
            <a href="index.php"><div class='logo'>
                <img src="img/logo.png">
                <div>
                    <p>Дополнительное профессиональное образование</p>
                    <p>Московский политех</p>
                </div>
            </div></a>
            <ul class="menu">
                <p>+7 (495) 223-05-21, dpo@mospolytech.ru</p>
                <li><a href="catalog.php">Каталог программ</a></li>
                <li><a href="calendar.php">Календарь обучения</a></li>
                <li><a href="about.php">О центре</a></li>
                <li><a href="news.php">Новости</a></li>
                <li><a href="signin.php">Записаться</a></li>
                <li><a href="contacts.php">Контакты</a></li>
            </ul>

            <!-- Pushy Menu -->
            <nav class="pushy pushy-left" data-focus="#first-link">
                <div class="pushy-content">
                    <ul>
                        <li class="pushy-link"><a href="index.php">Главная</a></li>
                        <li class="pushy-link"><a href="catalog.php">Каталог программ</a></li>
                        <li class="pushy-link"><a href="calendar.php">Календарь обучения</a></li>
                        <li class="pushy-link"><a href="about.php">О центре</a></li>
                        <li class="pushy-link"><a href="news.php">Новости</a></li>
                        <li class="pushy-link"><a href="signin.php">Записаться</a></li>
                        <li class="pushy-link"><a href="contacts.php">Контакты</a></li>
                        <li><a href="bank_documents.php">Банковские реквизиты</a></li>
                    </ul>
                </div>
            </nav>
            <a class="menu-btn">&#9776;</a>

        </div>
        <div class="header_text">
            <h1>Пройди курсы ДПО у нас!</h1>
            <p>Стань профессионалом в своем деле!</p>
            <div><a  href="signin.php">Записаться на курс</a></div>
        </div>
    </header>

    <main>
        <div class="slider_popular">
            <div class="populartoday">
                <div class="populartoday_text">
                    <h1>Популярные курсы</h1>
                    <p>Центр ДПО Московского политеха ведет обучение по самым востребованным программам дополнительного образования.  Мы готовим слушателей по программам в сфере промышленной, экологической и информационной безопасности и многих других.</p>
                </div>
            </div>

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <a href="">
                            <img src="img/prog_products.png" alt="Работа с программными продуктами">
                            <div class="carousel-caption">
                                <h2>Работа с программными продуктами</h2>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="">
                            <img src="img/safety.png" alt="Обеспечение безопасности дорожного движения">
                            <div class="carousel-caption">
                                <h2>Обеспечение безопасности дорожного движения</h2>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="">
                            <img src="img/car.png" alt="Эксплуатация и техническое обслуживание автомобилей">
                            <div class="carousel-caption">
                                <h2>Эксплуатация и техническое обслуживание автомобилей</h2>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="">
                            <img src="img/eco.png" alt="Промышленная и экологическая безопасность">
                            <div class="carousel-caption">
                                <h2>Промышленная и экологическая безопасность</h2>
                            </div>
                        </a>  
                    </div>
                    <div class="carousel-item">
                        <a href="">
                            <img src="img/metal.png" alt="Металлообработка">
                            <div class="carousel-caption">
                                <h2>Металлообработка</h2>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="">
                            <img src="img/expert.png" alt="Экспертная и оценочная деятельность">
                            <div class="carousel-caption">
                                <h2>Экспертная и оценочная деятельность</h2>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="">
                            <img src="img/language.png" alt="Лингвистика">
                            <div class="carousel-caption">
                                <h2>Лингвистика</h2>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="">
                            <img src="img/economy.png" alt="Экономика и управление">
                            <div class="carousel-caption">
                                <h2>Экономика и управление</h2>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="">
                            <img src="img/programming.png" alt="Ворлдскиллс Россия">
                            <div class="carousel-caption">
                                <h2>Ворлдскиллс Россия</h2>
                            </div>
                        </a>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div class="innumbers">
            <h1>Мы в цифрах</h1>
            <div class="innumbers_blocks">
                <div>
                    <img src='img/human.png'>
                    <p class="number"> 1000</p>
                    <p class="number_name">Преподавателей!</p>
                </div>
                <div>
                    <img src='img/gears.png'>
                    <p class="number">200</p>
                    <p class="number_name">Различных курсов!</p>
                </div>
                <div>
                    <img src='img/sertificate.png'>
                    <p class="number">10000</p>
                    <p class="number_name">Выпускников!</p>
                </div>
            </div>
        </div>

        <div class="whyimportant">
            <div>
                <h1>Почему важно повышать квалификацию?</h1>
                <p>Сейчас на рынке труда с каждым днем нужны все более квалифицированные специалисты. Чтобы продвигаться вверх по карьерной лестнице, необходимо постоянно развиваться и совершенствовать свои навыки. В этом помогут курсы ДПО Московского политеха.</p>
            </div>
            <img src="img/career.jpg">
        </div> 
        
        <div class="nextcourses">
            <h1>Ближайшие курсы</h1>
            <div class="calendar">
            <?php
                $host = 'std-mysql'; 
                $database = 'std_234'; 
                $user = 'std_234'; 
                $password = 'popygai18'; 
            
                $link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link));
                
                $query = "SELECT  * FROM DPO WHERE DateStart BETWEEN CURDATE() AND curdate() + interval 12 month ORDER BY DateStart ASC LIMIT 2";

                if ($result = $link->query($query)) {
                    while ($row = $result->fetch_assoc()) {
                        
                        setlocale(LC_ALL, 'ru_RU.UTF-8');
                        $old_date = $row['DateStart'];       
                        $old_date_timestamp = strtotime($old_date);
                        $new_date = strftime('%A, %e, %B ', $old_date_timestamp);  
                    
                        echo "<div class='calendar_item'>";
                        echo "<div class='date'>";
                        echo "<p>Начало: <b>";
                        echo $new_date;
                        echo "</b></p>";
                        echo "</div>";
                        echo "<div class='info'>";
                        
                            echo "<h3>";
                            echo $row['Name'];
                       
                        echo "</div>";
                        // echo "<button type='button' class='more_btn'>Подробнее</button>";
                        echo "</div>";                    
                    }
                    $result->free();
                }
            ?>
            </div>
            <div class="calendar_btn_div">
                <a href="calendar.php" class="calendar_btn">Календарь обучения</a>
            </div>
        </div>
            
            <a id="success"></a>
            <div class="mainpage_registration_form">
                <h1>Запишись на курс онлайн</h1>
                <form class="mainpage_r_form" enctype="multipart/form-data" name="request" method="post" action="php_forms/mainpage_form.php">
					<div>
						<label for="fio">ФИО:</label>
						<input name="fio" type="text" id="fio" placeholder="Введите ФИО" required>
					</div>
					
					<div>
						<label for="email">Email:</label>
						<input name="email" type="email" id="email" placeholder="Введите ваш email" required>
					</div>
					
					<div>
						<label for="number">Телефон:</label>
						<input name="number" type="telephone" id="number" placeholder="Введите ваш номер телефона" required>
                    </div>
                    <div>
                        <label for="select_face">Кто вы?</label>
                        <select id="select_face" name="select_face" required>
                                <option>Физическое лицо</option>
                                <option>Юридическое лицо</option>
                        </select>
                    </div>

					<div class='d-flex justify-content-center'>
						<div class="btn_mainpage_r_form">
							<button type="submit" name="button_request" class="btn">Отправить</button>
						</div>
                    </div>

                    <?php 
                        if (isset($_GET['formSend'])) {
                            echo '<p class="formSend_alert">Ваша заявка успешно отправлена! Мы скоро с вами свяжемся!</p>';
                        }
                        if (isset($_GET['formNotSend'])) {
                            echo '<p class="formNotSend_alert">Ваша заявка не была отправлена. Пожалуйста, попробуйте еще раз!</p>';
                        }
                    ?>
                    
                    <div class="personal_agree">
                        <p>Нажимая эту кнопку, Вы даете Московскому политехническому университету согласие на сбор и обработку Ваших персональных данных. Ознакомлен с Положением Московского Политеха об <a href="https://mospolytech.ru/storage/files/Polozhenie_v_otnoshenii_obrabotki_personalnyh_dannyh__(377893_v2_).pdf">обработке и защите персональных данных.</a></p>
                    </div>
                    
				</form>
        </div>
    </main>

    <footer>
        <?php include "footer.html"; ?>
    </footer>


    <div id="toTop">Наверх</div>

    <!-- Скрипты bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
    <!-- Pushy JS -->
    <script src="js/js_menu_pushy/pushy.min.js"></script>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Скрипты для кнопки "наверх" -->
    <script src="js/totopbutton.js"></script>
   
</body>

</html>