<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/header.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/totopbutton.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pushy.css">

    <link rel="shortcut icon" href="img/icon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

    <link href="css/bank_documents.css" rel="stylesheet">

    <title>ДПО Политех - Банковские реквизиты</title>
</head>
<body>
    <header>
        <?php include "header.html"; ?>
    </header>

    <main>
        <h1>Банковские реквизиты</h1>

        <table class="table table-hover">
            <tbody>
                <tr>
                    <td class="bold">Полное наименование:</td>
                    <td>федеральное государственное бюджетное образовательное учреждение высшего образования «Московский политехнический университет»</td>
                </tr>
                <tr>
                    <td class="bold">Краткое наименование:</td>
                    <td>Московский Политех</td>
                </tr>
                <tr>
                    <td class="bold">Получатель платежа (банковский реквизит):</td>
                    <td>УФК по г. Москве (Московский Политех л/с 20736Е04980) - буква Е в номере счета – русская.
                        <br>«Политех» указывать с заглавной буквы</td>
                </tr>
                <tr>
                    <td class="bold">Адрес:</td>
                    <td>107023 г. Москва, ул. Большая Семеновская, д.38</td>
                </tr>
                <tr>
                    <td class="bold">ОКТМО:</td>
                    <td>45314000</td>
                </tr>
                <tr>
                    <td class="bold">ОКАТО:</td>
                    <td>45263588000</td>
                </tr>
                <tr>
                    <td class="bold">ИНН:</td>
                    <td>7719455553</td>
                </tr>
                <tr>
                    <td class="bold">КПП:</td>
                    <td>771901001</td>
                </tr>
                <tr>
                    <td class="bold">ОГРН:</td>
                    <td>1167746817810</td>
                </tr>
                <tr>
                    <td class="bold">Расчетный счет:</td>
                    <td>40501810845252000079</td>
                </tr>
                <tr>
                    <td class="bold">КБК (код бюджетной классификации). Обязательно заполняется в платежном поручение в поле 104, или в назначении платежа):</td>
                    <td>00000000000000000130 - платные образовательные услуги, коммунальные услуги, трудовые книжки
                        <br>00000000000000000120 - аренда (в т.ч. НДС с аренды)
                        <br>00000000000000000140 – пени за просрочку оплаты обучения
                        <br>00000000000000000410 – возмещение за утерю книг</td>
                </tr>
                <tr>
                    <td class="bold">Лицевой счет:</td>
                    <td>20736Е04980 (примечание: буква Е в номере счета - русская)</td>
                </tr>
                <tr>
                    <td class="bold">Банк:</td>
                    <td>ГУ Банка России по ЦФО</td>
                </tr>
                <tr>
                    <td class="bold">БИК:</td>
                    <td>044525000</td>
                </tr>
                <tr>
                    <td class="bold">ОКПО:</td>
                    <td>04350607</td>
                </tr>
                <tr>
                    <td class="bold">Ректор:</td>
                    <td>Миклушевский Владимир Владимирович</td>
                </tr>
                <tr>
                    <td class="bold">Главный бухгалтер:</td>
                    <td>Бондарь Светлана Васильевна</td>
                </tr>
                <tr>
                    <td class="bold">Телефон/факс бухгалтерия:</td>
                    <td>(495) 223-05-23</td>
                </tr>
            </tbody>
        </table>
        
    </main>

     <footer>  
        <?php include "footer.html"; ?>
    </footer>

    <div id="toTop">Наверх</div>

    <!-- Скрипты bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
    <!-- Pushy JS -->
    <script src="js/js_menu_pushy/pushy.min.js"></script>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Скрипты для кнопки "наверх" -->
    <script src="js/totopbutton.js"></script>
</body>

</html>