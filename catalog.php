﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/header.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/totopbutton.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pushy.css">

    <link rel="shortcut icon" href="img/icon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

    <link href="css/catalog.css" rel="stylesheet">
   
    <title>ДПО Политех - Каталог программ</title>
</head>
<body>
    <header>
        <?php include "header.html"; ?>
    </header>

    <main>
        <div class="wrapper">
            <div class="inner-wrapper">
                <h1>Поиск курсов</h1>
                <form method="POST" action="php_forms/filter.php">
                    <div class="check">
                        <label><input type="checkbox" name="ochnoe" value="1"><span class="checkbox-custom"></span>Очное</label>
                        <label><input type="checkbox" name="ochno-zaochnoe" value="2"><span class="checkbox-custom"></span>Очно-заочное</label>
                        <label><input type="checkbox" name="zaochnoe" value="3"><span class="checkbox-custom"></span>Заочное</label>
                        <label><input type="checkbox" name="distant" value="4"><span class="checkbox-custom"></span>Дистанционное</label>
                    </div>
                    <div class="buttons"> 
                        <!-- <button type="button" class="btn  btn-lg bbo" data-toggle="modal" data-target="#myModal1">Сферы обучения</button> -->
                        <a href="" data-toggle="modal" data-target="#myModal1">Сферы обучения <img style="width: 15px" src="img/more.png"></a>
                        
                        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Сферы обучения</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>                                    
                                    </div>
                                    <div class="modal-body specialty">
                                           <label><input type="checkbox" name="option1" value="1">Детский технопарк</label>
                                           <label><input type="checkbox" name="option2" value="2">Промышленность и экологическая безопасность</label>
                                           <label><input type="checkbox" name="option3" value="3">Обеспечение безопасности дорожного движения</label>
                                           <label><input type="checkbox" name="option4" value="4">Эксплуатация и техническое обслуживание атомобилей</label>
                                           <label><input type="checkbox" name="option5" value="5">Работа с программными продуктами</label>
                                           <label><input type="checkbox" name="option6" value="6">Экспертная и оценочная деятельность</label>
                                           <label><input type="checkbox" name="option7" value="7">Охрана труда и энергоэффективность</label>
                                           <label><input type="checkbox" name="option8" value="8">Перевозка опасных грузов</label>
                                           <label><input type="checkbox" name="option9" value="9">Строительство</label>
                                           <label><input type="checkbox" name="option10" value="10">Металлообработка</label>
                                           <label><input type="checkbox" name="option11" value="11">Экономика и управление</label>
                                           <label><input type="checkbox" name="option12" value="12">Образование и педагогика</label>
                                           <label><input type="checkbox" name="option13" value="13">Лингвистика</label>
                                           <label><input type="checkbox" name="option14" value="14">Ворлдскиллс Россия</label>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn bbo" class="close" data-dismiss="modal">Применить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- <button type="button" class="btn  btn-lg bbo" data-toggle="modal" data-target="#myModal">Условия обучения</button> -->
                        <a href="" data-toggle="modal" data-target="#myModal">Условия обучения <img style="width: 15px" src="img/more.png"></a>
                        
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Условия обучения</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>                                    
                                    </div>
                                    <div class="modal-body">
                                        <div class="price">
                                            <p>Максимальная цена: <span id="rangeValue">100000</span>руб.</p>
                                            <input type="range" name="maxprice" min=0 max="100000" step="500"  onchange="document.getElementById('rangeValue').innerHTML = this.value;" value="100000" />
                                        </div>
                                        <div class="pl" >
                                            <p>Площадки:</p>
                                            <label><input type="checkbox" name="Semenovskaya" value="Semenovskaya">Ул. Большая Семеновская, 38 (метро «Электрозаводская»)</label>
                                            <label><input type="checkbox" name="Avtozavodskaya" value="Avtozavodskaya">Ул. Автозаводская, 16 (метро «Автозаводская») </label>
                                            <label><input type="checkbox" name="Pavla_Korchagina" value="Pavla_Korchagina">Ул. Павла Корчагина, 22 (метро «ВДНХ»)</label>
                                            <label><input type="checkbox" name="Dubrovkaya" value="Dubrovkaya">Ул. 1-я Дубровская, д. 16а (метро «Дубровка»)</label>
                                        </div>
                                        <div class="vidi_obuch" >
                                            <p>Виды обучения:</p>
                                            <label><input type="checkbox" name="Povishenie" value="Povishenie">Повышение квалификации</label>
                                            <label><input type="checkbox" name="Perepodgotovka" value="Perepodgotovka">Профессиональная переподготовка</label>
                                            <label><input type="checkbox" name="Dop_program" value="Dop_program">Дополнительные образовательные программы</label>
                                            <label><input type="checkbox" name="Prof_raboch" value="Prof_raboch">Профессии рабочих, должности служащих</label>
                                        </div>
                                        <div class="competencii" >
                                            <p>Формируемые компетенции:</p>
                                            <label><input type="checkbox" name="Bazovie" value="Bazovie">Базовые компетенции</label>
                                            <label><input type="checkbox" name="Prof" value="Prof">Профессиональные компетенции</label>
                                            <label><input type="checkbox" name="Lichnostnie" value="Lichnostnie">Личностные компетенции</label>
                                        </div>
                                        <div class="date">
                                            <p>Дата обучения:</p>
                                                <label for="date1">С</label>
                                                <input type="date" id="date1" name="date1" value="<?php date('Y-m-d'); ?>"/>
                                                <br>
                                                <label for="date2">По</label>
                                                <input type="date" id="date2" name="date2" value="<?php date('Y-m-d'); ?>"/>   
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn bbo"  class="close" data-dismiss="modal">Применить</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    <button type="submit" name="submit" class="btn bbo submit_btn" value="submit">Применить</button>
                        
                        <!-- $cancel=$_POST['cancel'];
                        
                        if(!isset($cancel)) {
                            
                            echo '<form method="POST">';
                            echo '<button type="button" name="cancel" class="btn bbo submit_btn">Сбросить</button>';
                            echo '</form>';

                        }
                        
                        if(isset($cancel)) { 
                            header('Location: ../catalog.php');  

                        } -->
                    
                    
                            
                        


                </form>
            </div>
        </div>


        <div class="courses">
            <table class="table table-hover">
                <thead>
                    <tr>
                    <th class="img"></th>
                    <th>Название</th>
                    <th class="vid_lable">Форма</th>
                    <th class="speaker_lable">Спикер</th>
                    <th class="time_lable">Длительность</th>
                    <th>Стоимость обучения</th>
                    </tr>
                </thead>
                <tbody> 
                    <?php
                        $host = 'std-mysql'; 
                        $database = 'std_234'; 
                        $user = 'std_234'; 
                        $password = 'popygai18'; 
                        
                        $link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link)); 
                        
                       
                        if(!isset($_GET['form']) || ( $_GET['sphere'] == '0' & $_GET['place'] == '0000' & $_GET['form'] == '0000' & $_GET['vid'] == '0000' & $_GET['comp'] == '000' & $_GET['max'] == '100000' & !isset($_GET['from']) & !isset($_GET['to']))) {
                            $query = "SELECT * FROM DPO"; 
                        } else {
                            $query = "SELECT * FROM DPO WHERE";
                        }



                        


                        //фильтр по формам обучения
                        $flag_form=false;
                        if(isset($_GET['form']))  {
                            
                            $form=$_GET['form'];

                            if ($form[0]==1) {
                                $query .= " Form = 'Очная'";
                                $flag_form=true;
                            }
                            if ($form[1]==1) {
                                if ($flag_form) {
                                    $query .= " OR Form = 'Очно-заочная'"; 
                                } else {
                                    $query .= " Form = 'Очно-заочная'";
                                    $flag_form=true; 
                                }
                            }
                            if ($form[2]==1) {
                                if ($flag_form) {
                                    $query .= " OR Form = 'Заочная'";
                                } else {
                                    $query .= " Form = 'Заочная'"; 
                                    $flag_form=true; 
                                } 
                            }
                            if ($form[3]==1) {
                                if ($flag_form) {
                                    $query .= " OR Form = 'Дистанционная'";
                                } else {
                                    $query .= " Form = 'Дистанционная'"; 
                                    $flag_form=true; 
                                } 
                            }
                        } 
                        
                        //фильтр по сферам обучения
                        $flag=false;
                        if(isset($_GET['sphere']) && $_GET['sphere'] !== '0')    {

                            $sphere_bin = decbin ($_GET['sphere']);
                            for ($i=1; 14-strlen($sphere_bin); $i++) {
                                $sphere_bin='0'.$sphere_bin;
                            }
                            
                            if ($flag_form) {
                                $query .= " AND ( ";
                            }


                            if ($sphere_bin[0]==1) {
                                $query .= " Sphere = 'Детский технопарк'";
                                $flag=true;
                            } 
                            if ($sphere_bin[1]==1) {
                                if ( $flag)  {
                                $query .= " OR Sphere = 'Промышленность и экологическая безопасность'";
                                } else {
                                    $query .= " Sphere = 'Промышленность и экологическая безопасность'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[2]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Обеспечение безопасности дорожного движения'";
                                } else {
                                    $query .= " Sphere = 'Обеспечение безопасности дорожного движения'";
                                    $flag=true;
                                }
                            } 
                            if ($sphere_bin[3]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Эксплуатация и техническое обслуживание атомобилей'";
                                } else {
                                    $query .= " Sphere = 'Эксплуатация и техническое обслуживание атомобилей'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[4]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Работа с программными продуктами'";
                                } else {
                                    $query .= " Sphere = 'Работа с программными продуктами'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[5]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Экспертная и оценочная деятельность'";
                                } else {
                                    $query .= " Sphere = 'Экспертная и оценочная деятельность'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[6]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Охрана труда и энергоэффективность'";
                                } else {
                                    $query .= " Sphere = 'Охрана труда и энергоэффективность'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[7]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Перевозка опасных грузов'";
                                } else {
                                    $query .= " Sphere = 'Перевозка опасных грузов'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[8]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Строительство'";
                                } else {
                                    $query .= " Sphere = 'Строительство'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[9]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Металлообработка";
                                } else {
                                    $query .= " Sphere = 'Металлообработка'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[10]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Экономика и управление";
                                } else {
                                    $query .= " Sphere = 'Экономика и управление'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[11]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Образование и педагогика";
                                } else {
                                    $query .= " Sphere = 'Образование и педагогика'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[12]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Лингвистика";
                                } else {
                                    $query .= " Sphere = 'Лингвистика'";
                                    $flag=true;
                                }
                            }
                            if ($sphere_bin[13]==1) {
                                if ($flag) {
                                    $query .= " OR Sphere = 'Ворлдскиллс Россия";
                                } else {
                                    $query .= " Sphere = 'Ворлдскиллс Россия'";
                                    $flag=true;
                                }
                            }
                            

                            if ($flag_form) {
                                $query .= " ) ";
                            }
                        } 

                        //фильтр по площадкам проведения
                        $flag_place=false;
                        if(isset($_GET['place']) && $_GET['place'] !== "0000")    {

                            if ($flag_form || $flag) {
                                $query .= " AND ( ";
                            }
                       
                            $place=$_GET['place'];

                            if ($place[0]==1) {
                                $query .= " Place = 'Семеновская'";
                                $flag_place=true;
                            }
                            if ($place[1]==1) {
                                if ($flag_place) {
                                $query .= " OR Place = 'Автозаводская'"; 
                                } else {
                                    $query .= " Place  = 'Автозаводская'"; 
                                    $flag_place=true;
                                }
                            }
                            if ($place[2]==1) {
                                if ($flag_place) {
                                    $query .= " OR Place  = 'Павла Корчагина'";
                                } else {
                                    $query .= " Place  = 'Павла Корчагина'"; 
                                    $flag_place=true;
                                } 
                            }
                            if ($place[3]==1)  {
                                if ($flag_place) {
                                    $query .= " OR Place  = 'Дубровская'";
                                } else {
                                    $query .= " Place  = 'Дубровская'"; 
                                    $flag_place=true;
                                } 
                            }


                            if ($flag_form || $flag) {
                                $query .= " ) ";
                            }
                        } 
                        

                        //фильтр по видам обучения
                        $flag_vid=false;
                        if(isset($_GET['vid']) && $_GET['vid'] !== "0000")    {

                            if ($flag_form || $flag || $flag_place) {
                                $query .= " AND ( ";
                            }
                       
                            $vid=$_GET['vid'];

                            if ($vid[0]==1) {
                                $query .= " Vid = 'Повышение квалификации'";
                                $flag_vid=true;
                            }
                            if ($vid[1]==1) {
                                if ($flag_vid) {
                                $query .= " OR Vid = 'Профессиональная переподготовка'"; 
                                } else {
                                    $query .= " Vid  = 'Профессиональная переподготовка'"; 
                                    $flag_vid=true;
                                }
                            }
                            if ($vid[2]==1) {
                                if ($flag_vid) {
                                    $query .= " OR Vid = 'Дополнительные образовательные программы'";
                                } else {
                                    $query .= " Vid = 'Дополнительные образовательные программы'"; 
                                    $flag_vid=true;
                                } 
                            }
                            if ($vid[3]==1)  {
                                if ($flag_vid) {
                                    $query .= " OR Vid = 'Профессии рабочих, должности служащих'";
                                } else {
                                    $query .= " Vid = 'Профессии рабочих, должности служащих'"; 
                                    $flag_vid=true;
                                } 
                            }


                            if ($flag_form || $flag || $flag_place) {
                                $query .= " ) ";
                            }
                        } 
                        

                         //фильтр по формируемым компетенциям
                        $flag_comp=false;
                        if(isset($_GET['comp']) && $_GET['comp'] !== "000")    {

                            if ($flag_form || $flag || $flag_place || $flag_vid) {
                                $query .= " AND ( ";
                            }
                       
                            $comp=$_GET['comp'];

                            if ($comp[0]==1) {
                                $query .= " Comp = 'Базовые компетенции'";
                                $flag_comp=true;
                            }
                            if ($comp[1]==1) {
                                if ($flag_comp) {
                                $query .= " OR Comp = 'Профессиональные компетенции'"; 
                                } else {
                                    $query .= " Comp  = 'Профессиональные компетенции'"; 
                                    $flag_comp=true;
                                }
                            }
                            if ($comp[2]==1) {
                                if ($flag_comp) {
                                    $query .= " OR Comp = 'Личностные компетенции'";
                                } else {
                                    $query .= " Comp = 'Личностные компетенции'"; 
                                    $flag_comp=true;
                                } 
                            }

                            if ($flag_form || $flag || $flag_place || $flag_vid) {
                                $query .= " ) ";
                            }
                        } 


                        //фильтр по дате
                        $flag_date=false;
                        if(isset($_GET['from']) || isset($_GET['to']))    {

                            if ($flag_form || $flag || $flag_place || $flag_vid || $flag_comp) {
                                $query .= " AND ";
                            }
                       
                            if(isset($_GET['from'])) {
                                $from=date('Y-m-d', strtotime($_GET['from']));
                                $query .= " DateStart = ".$from."";
                                $flag_date=true;
                            }

                            if(isset($_GET['to'])) {
                                $to=date('Y-m-d', strtotime($_GET['to']));
                                if ($flag_date) {
                                    $query .= " AND DateEnd = ".$to."";
                                } else {
                                    $query .= " DateEnd = ".$to."";
                                    $flag_date=true;
                                }   
                            }  
                        } 

                        //фильтр по цене
                        if(isset($_GET['max']) && $_GET['max'] !== '100000')    {

                            if ($flag_form || $flag || $flag_place || $flag_vid || $flag_comp || $flag_date) {
                                $query .= " AND ";
                            }
                            
                            $max=$_GET['max'];

                            $query .= " Price = ".$max."";
                            
                        } 


                        $query .= " ORDER BY Name ASC";
                        // echo $query;
                       
                        if ($result = $link->query($query)) {
                            while ($row = $result->fetch_assoc()) {
                                 
                                echo '<div class="course-element">';
                                echo '<tr>';
                                echo'<th scope="row" class="img"><img src="img/calendar.png"></th>';
                                echo '<td class="name"><a href="">';
                                echo $row["Name"];
                                echo '</a></td>';
                                echo '<td class="vid">';
                                echo $row['Form'];
                                echo '</td>';  
                                echo '<td class="speaker">';
                                echo $row['Speaker'];
                                echo '</td>';
                                echo '<td class="time">';
                                echo $row['Duration'];
                                echo '</td>';
                                echo '<td class="pr">';
                                echo $row['Price'];
                                echo '</td>';
                                echo '</tr>';
                                echo '</div>';
                                   
                            }
                            $result->free();
                         }
                    
                    ?>
                </tbody>
            </table>
        </div>

 
    </main>

     <footer>  
        <?php include "footer.html"; ?>
    </footer>

    <div id="toTop">Наверх</div>
    
    <!-- Скрипты bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
    <!-- Pushy JS -->
    <script src="js/js_menu_pushy/pushy.min.js"></script>
   
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Скрипты для кнопки "наверх" -->
    <script src="js/totopbutton.js"></script>
   
</body>

</html>