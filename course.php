<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/header.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/totopbutton.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pushy.css">

    <link rel="shortcut icon" href="img/icon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

    <link href="css/course.css" rel="stylesheet">

    <title>ДПО Политех - Курс</title>
</head>
<body>
    <header>
        <?php include "header.html"; ?>
    </header>

    <main>
        <h1 class="name_course">Название курса</h1>
        <div class="course_header">
            <div class="course_info">
                <p><b>Направление:</b> такое-то направление.</p>
                <p><b>Стоимость:</b> 10 000 рублей.</p>
                <p><b>Объем часов:</b> 36 часов.</p>
                <p><b>Выдаваемый документ:</b> такой-то документ.</p>
            </div>
            <div class="speaker">
                <h1>Спикер:</h1>
                <img src="img/man.png">
                <p class="name">Югансон Анна Райвовна</p>
                <p class="job">Кандидат философских наук</p>
            </div>
        </div>
        <div class="about_course">
            <h1>О программе</h1>
            <div>
                <img src="img/whyimportant.jpg">
                <p>Много текста. Много текста. Текст, текст, текст аахах привет как дела здесь должен быть текст. Много текста. Много текста. Текст, текст, текст аахах привет как дела здесь должен быть текст. Много текста. Много текста. Текст, текст, текст аахах привет как дела здесь должен быть текст. Много текста. Много текста. Текст, текст, текст аахах привет как дела здесь должен быть текст. Много текста. Много текста. Текст, текст, текст аахах привет как дела здесь должен быть текст.</p>
            </div>
        </div>

        <div class="program">
            <h1>Учебный план</h1>            
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Первый этап
                        </button>
                    </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                        <p>Описание этапа</p>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Второй этап
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>Описание этапа</p>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Третий этап
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>Описание этапа</p>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Четвертый этап
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                        <p>Описание этапа</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="comments">
            <h1>Отзывы о курсе</h1>
            <div class="comments_item">
                <h2>Андрей</h2>
                <h3>22.08.2018, 23:55</h3>
                <p>Хороший курс</p>
            </div>
        </div>  
        <div class="add_comment">
            <h1>Написать отзыв:</h1>
            <form class="add_comment_form" enctype="multipart/form-data" name="request" method="post" action="php_forms/add_comment_form.php"> 
                <div>
                    <label for="name">Имя:</label>
                    <input name="name" type="text" id="name" placeholder="Введите ваше имя">
                </div>
                            
                <div>
                    <label for="comment">Отзыв:</label>
                    <textarea name="comment" id="comment" type="text" placeholder="Ваш отзыв"></textarea>
                </div>
                <div class='d-flex justify-content-center'>
                    <div class="btn_add_comment_form">
                        <button type="submit" name="button_request" class="btn">Отправить</button>
                    </div>
                </div>
            </form>
        </div>


        <div class="coursepage_registration">
            <h1>Запишись на курс онлайн</h1>
            <div class="coursepage_registration_form">
                <form class="coursepage_r_form" enctype="multipart/form-data" name="request" method="post" action="php_forms/coursepage_registration_form.php">					
                    <div>
                        <label for="fio">ФИО:</label>
                        <input name="fio" type="text" id="fio" placeholder="Введите ФИО">
                    </div>
                        
                    <div>
                        <label for="email">Email:</label>
                        <input name="email" type="email" id="email" placeholder="Введите ваш email">
                    </div>
                        
                    <div>
                        <label for="number">Телефон:</label>
                        <input name="number" type="telephone" id="number" placeholder="Введите ваш номер телефона">
                    </div>
                    <div>
                        <label for="select_face">Кто вы?</label>
                        <select id="select_face">
                                <option>Физическое лицо</option>
                                <option>Юридическое лицо</option>
                        </select>
                    </div>

                    <div class='d-flex justify-content-center'>
                        <div class="btn_coursepage_r_form">
                            <button type="submit" name="button_request" class="btn">Отправить</button>
                        </div>
                    </div>
                    <div class="personal_agree">
                        <p>Нажимая эту кнопку, Вы даете Московскому политехническому университету согласие на сбор и обработку Ваших персональных данных. Ознакомлен с Положением Московского Политеха об <a href="https://mospolytech.ru/storage/files/Polozhenie_v_otnoshenii_obrabotki_personalnyh_dannyh__(377893_v2_).pdf">обработке и защите персональных данных.</a></p>
                    </div>
                </form>
            </div>
        </div>
    </main>

     <footer>  
        <?php include "footer.html"; ?>
    </footer>

    <div id="toTop">Наверх</div>

    <!-- Скрипты bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
    <!-- Pushy JS -->
    <script src="js/js_menu_pushy/pushy.min.js"></script>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Скрипты для кнопки "наверх" -->
    <script src="js/totopbutton.js"></script>
</body>

</html>