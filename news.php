<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/header.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/totopbutton.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pushy.css">

    <link rel="shortcut icon" href="img/icon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

    <link href="css/news.css" rel="stylesheet">

    <title>ДПО Политех - Новости</title>
</head>
<body>
    <header>
        <?php include "header.html"; ?>
    </header>

    <main>
        <h1>Новости</h1>
        <div class="news">
            <?php
                $host = 'std-mysql'; 
                $database = 'std_234'; 
                $user = 'std_234'; 
                $password = 'popygai18'; 
                
                $link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link));
                    
                $query = "SELECT * FROM DPO_NEWS ORDER BY DatePub DESC";

                    if ($result = $link->query($query)) {
                        while ($row = $result->fetch_assoc()) {
                            
                            setlocale(LC_ALL, 'ru_RU.UTF-8');
                            $old_date = $row['DatePub'];       
                            $old_date_timestamp = strtotime($old_date);
                            $new_date = strftime('%A, %e, %B %Y', $old_date_timestamp);  
                        
                            $text = $row['Text'];   
                            $rest = substr($text, 0, 260); //выводим первые 130 символов
 

                            echo "<div class='news_item'>";
                            
                                echo '<div class="wrapper">';
                                echo "<div class='title'>";        
                                echo $row['Title'];
                                echo "</div>";

                                echo "<div class='date'>";
                                echo "<p>Дата публикации: ";        
                                echo $new_date;
                                echo "</div>";    

                                echo "<div class='text'>";                                    
                                echo $rest;
                                echo "...</div>"; 
                                

                                echo "<div class='more'>";                                    
                                echo "<a href=''>Подробнее</a>";
                                echo "</div>";
                                echo "</div>";

                                
                                echo '<div class="img_news"><img src="img/expert.png"></div>';

                            echo "</div>";        
                        }
                        $result->free();
                    }
            ?>
        </div>
    </main>

    <footer>  
        <?php include "footer.html"; ?>
    </footer>

    <div id="toTop">Наверх</div>

    <!-- Скрипты bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
    <!-- Pushy JS -->
    <script src="js/js_menu_pushy/pushy.min.js"></script>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Скрипты для кнопки "наверх" -->
    <script src="js/totopbutton.js"></script>
</body>

</html>