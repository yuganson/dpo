<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/header.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/totopbutton.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pushy.css">

    <link rel="shortcut icon" href="img/icon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

    <link href="css/signin.css" rel="stylesheet">

    <title>ДПО Политех - Записаться</title>
</head>
<body>
    <header>
        <?php include "header.html"; ?>
    </header>

    <main>
        <h1>Подать заявку на обучение</h1>
        <form class="signin_r_form" enctype="multipart/form-data" name="request" method="post" action="php_forms/signin_form.php">
            <div>
                <label for="select_course">Выберите курс:</label>
                <select id="select_course" name="select_course">

                <?php
                    $host = 'std-mysql'; 
                    $database = 'std_234'; 
                    $user = 'std_234'; 
                    $password = 'popygai18'; 
                
                    $link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link));
                    
                    $query = "SELECT  * FROM DPO";

                    if ($result = $link->query($query)) {
                        while ($row = $result->fetch_assoc()) {
                           
                           echo '<option>';
                           echo $row['Name'];
                           echo '</option>';                    
                        }
                        $result->free();
                    }
                ?>

                </select>
            </div>
        
            <div>
				<label for="fio">ФИО:</label>
				<input name="fio" type="text" id="fio" placeholder="Введите ФИО">
			</div>
					
			<div>
				<label for="email">Email:</label>
				<input name="email" type="email" id="email" placeholder="Введите ваш email">
			</div>
					
			<div>
				<label for="number">Телефон:</label>
				<input name="number" type="telephone" id="number" placeholder="Введите ваш номер телефона">
           </div>
            
           <div>
               <label for="select_face">Кто вы?</label>
                <select id="select_face" name="select_face">
                    <option>Физическое лицо</option>
                    <option>Юридическое лицо</option>
                </select>
            </div>

			<div class='d-flex justify-content-center'>
				<div class="btn_signin_r_form">
					<button type="submit" name="button_request" class="btn">Отправить</button>
                </div>
                
            </div>

            <?php 
                if (isset($_GET['formSend'])) {
                    echo '<p class="formSend_alert">Ваша заявка успешно отправлена! Мы скоро с вами свяжемся!</p>';
                }
                if (isset($_GET['formNotSend'])) {
                    echo '<p class="formNotSend_alert">Ваша заявка не была отправлена. Пожалуйста, попробуйте еще раз!</p>';     
                }
            ?>
                    
            <div class="personal_agree">
                <p>Нажимая эту кнопку, Вы даете Московскому политехническому университету согласие на сбор и обработку Ваших персональных данных. Ознакомлен с Положением Московского Политеха об <a href="https://mospolytech.ru/storage/files/Polozhenie_v_otnoshenii_obrabotki_personalnyh_dannyh__(377893_v2_).pdf">обработке и защите персональных данных.</a></p>
            </div>

                    
        </form>
        <div class="links">
            <a href="bank_documents.php">БАНКОВСКИЕ РЕКВИЗИТЫ</a>
        </div> 
    </main>

     <footer>  
        <?php include "footer.html"; ?>
    </footer>

    <div id="toTop">Наверх</div>

    <!-- Скрипты bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
    <!-- Pushy JS -->
    <script src="js/js_menu_pushy/pushy.min.js"></script>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Скрипты для кнопки "наверх" -->
    <script src="js/totopbutton.js"></script>
</body>

</html>