<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/header.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/totopbutton.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pushy.css">
    <link rel="shortcut icon" href="img/icon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

    <link href="css/about.css" rel="stylesheet">

    <title>ДПО Политех - О нас</title>
</head>
<body>
    <header>
        <?php include "header.html"; ?>
    </header>
    <main>
        <!-- <h1>О центре</h1> -->
        <!-- <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        О нас
                    </button>
                </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <b>Центр профессиональной переподготовки и повышения квалификации Московского политехнического университета (Московский Политех)</b> ведет обучение по самым востребованным программам дополнительного образования. Мы готовим слушателей по программам в сфере промышленной, экологической и информационной безопасности, диагностики автомототранспортных средств и безопасности дорожного движения, охраны труда, оценочной деятельности, экономики, строительства и многих других.  
                        <br><b>ФГБОУ ВО «Московский политехнический университет»</b> - современный крупный многопрофильный государственный политехнический вуз. В соответствии с приказом Минобрнауки России от 21 марта 2016 г.  Московский Политех создан путем реорганизации в форме слияния Университета машиностроения (МАМИ) и Московского государственного университета печати имени Ивана Федорова (МГУП).
                        <br><br>Лицензия № 2398 от 22 сентября 2016 г. на осуществление образовательной деятельности.
                        <br>Свидетельство № 2292 от 11 октября 2016 г. о государственной аккредитации.
                        <div class="links">
                            <a href="bank_documents.php">БАНКОВСКИЕ РЕКВИЗИТЫ</a>
                            <br><a href="rights_documents.php">ПРАВОВЫЕ ДОКУМЕНТЫ</a>
                        </div>    
                        <div>
                            <img src="img/group1.jpg" class="aboutimg">
                            <img src="img/group2.jpg" class="aboutimg">
                            <img src="img/group3.jpg" class="aboutimg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            На базе какого университета?
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        ФГБОУ ВО «Московский политехнический университет» - современный крупный многопрофильный государственный политехнический вуз. В соответствии с приказом Минобрнауки России от 21 марта 2016 г.  Московский Политех создан путем реорганизации в форме слияния Университета машиностроения (МАМИ) и Московского государственного университета печати имени Ивана Федорова (МГУП).
                        <br><br>Лицензия № 2398 от 22 сентября 2016 г. на осуществление образовательной деятельности.
                        <br>Свидетельство № 2292 от 11 октября 2016 г. о государственной аккредитации.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Где проходит обучение?
                        </button>
                    </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>Ул. Большая Семеновская, 38 (метро «Электрозаводская»)</li>
                            <li>Ул. Автозаводская, 16 (метро «Автозаводская»)</li>
                            <li>Ул. Павла Корчагина, 22 (метро «ВДНХ»)</li>
                            <li>Ул. 1-я Дубровская, д. 16а (метро «Дубровка»)</li>
                        </ul>
                        <div class="map">
                            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa5aad5d82fb26d01db1de8be8bd3bad62adeef8a18a8cca3b5c92ef190d6a70e&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                    <div class="card-header" id="headingFour">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Какие документы мы выдаем?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                            <ul>
                                <li>Удостоверение о повышении квалификации</li>
                                <li>Диплом о профессиональной переподготовке</li>
                                <li>Свидетельство о профессии рабочего, должности служащего</li>
                                <li>Сертификат об обучении</li>
                            </ul>   
                        </div>
                    </div>
                </div>
        </div> -->
        <div>
            <div class="aboutus">
                <h1>О нас</h1>
                <p><b>Центр профессиональной переподготовки и повышения квалификации Московского политехнического университета (Московский Политех)</b> ведет обучение по самым востребованным программам дополнительного образования. Мы готовим слушателей по программам в сфере промышленной, экологической и информационной безопасности, диагностики автомототранспортных средств и безопасности дорожного движения, охраны труда, оценочной деятельности, экономики, строительства и многих других.  
                <br><b>ФГБОУ ВО «Московский политехнический университет»</b> - современный крупный многопрофильный государственный политехнический вуз. В соответствии с приказом Минобрнауки России от 21 марта 2016 г.  Московский Политех создан путем реорганизации в форме слияния Университета машиностроения (МАМИ) и Московского государственного университета печати имени Ивана Федорова (МГУП).
                <br><br>Лицензия № 2398 от 22 сентября 2016 г. на осуществление образовательной деятельности.
                <br>Свидетельство № 2292 от 11 октября 2016 г. о государственной аккредитации.</p>
                <div class="links">
                    <a href="bank_documents.php">БАНКОВСКИЕ РЕКВИЗИТЫ</a>
                    <!-- <br><a href="rights_documents.php">ПРАВОВЫЕ ДОКУМЕНТЫ</a> -->
                </div>    
                <div>
                    <img src="img/group1.jpg" class="aboutimg">
                    <img src="img/group2.jpg" class="aboutimg">
                    <img src="img/group3.jpg" class="aboutimg">    
                </div>
            </div>
            <div class="where">
                <h1>На каких площадках проходит обучение?</h1>
                <ul>
                    <li>Ул. Большая Семеновская, 38 (метро «Электрозаводская»)</li>
                    <li>Ул. Автозаводская, 16 (метро «Автозаводская»)</li>
                    <li>Ул. Павла Корчагина, 22 (метро «ВДНХ»)</li>
                    <li>Ул. 1-я Дубровская, д. 16а (метро «Дубровка»)</li>
                </ul>
                <div class="map">
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa5aad5d82fb26d01db1de8be8bd3bad62adeef8a18a8cca3b5c92ef190d6a70e&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
            </div>
            <div class="certificates">
                <h1>Какие документы мы выдаем?</h1>
                <ul>
                    <li>Удостоверение о повышении квалификации</li>
                    <li>Диплом о профессиональной переподготовке</li>
                    <li>Свидетельство о профессии рабочего, должности служащего</li>
                    <li>Сертификат об обучении</li>
                </ul>   
            </div>
        </div>
    </main>
    <footer>  
        <?php include "footer.html"; ?>
    </footer>

    <div id="toTop">Наверх</div>

    <!-- Скрипты bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
    <!-- Pushy JS -->
    <script src="js/js_menu_pushy/pushy.min.js"></script>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Скрипты для кнопки "наверх" -->
    <script src="js/totopbutton.js"></script>
</body>

</html>