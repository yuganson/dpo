<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/header.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/totopbutton.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pushy.css">

    <link rel="shortcut icon" href="img/icon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

    <link href="css/contacts.css" rel="stylesheet">

    <title>ДПО Политех - О нас</title>
</head>
<body>
    <header>
        <?php include "header.html"; ?>
    </header>

    <main>
        <h1>Коллектив центра</h1>
        <div class="team">
            <div class="colleague">
                <div>
                    <h4>Дубинина Екатерина Викторовна</h4>
                    <p>Начальник центра</p>
                    <p>Телефон: 1324</p>
                    <p>Эл.почта: e.v.dubinina@mospolytech.ru</p>
                </div>
                <img src="img/woman.png">
            </div>
            <div class="colleague">
                <div>
                    <h4>Жигар Анастасия Сергеевна</h4>
                    <p>Заместитель начальника центра по общим вопросам</p>
                    <p>Телефон: 1524</p>
                    <p>Эл.почта: a.s.zhigar@mospolytech.ru</p>
                </div>
                <img src="img/woman.png">
            </div>
            <div class="colleague">
                <div>
                    <h4>Малышев Роман Михайлович</h4>
                    <p>Заместитель начальника центра по направлению экологическая и промышленная безопасность</p>
                    <p>Телефон: 8-916-507-50-76</p>
                    <p>Эл.почта: ipb_msuie@mail.ru</p>
                </div>
                <img src="img/man.png">
            </div>
        </div>

        <div class="adress">
            <h1>Адрес центра</h1>
            <p>107023, город Москва, улица Большая Семеновская, дом 38, корпус А, этаж 4, кабинет 422</p>
            <div class="map">
                <!-- <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A8aa631c4e1c0f9e5575d1674f1a4f7e53c7c4d95d28aff4105c7929b3a7f672f&amp;width=500&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script> -->
                <!-- <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A8aa631c4e1c0f9e5575d1674f1a4f7e53c7c4d95d28aff4105c7929b3a7f672f&amp;width=700&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script> -->
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A8aa631c4e1c0f9e5575d1674f1a4f7e53c7c4d95d28aff4105c7929b3a7f672f&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
        <div class="grafic">
            <h1>График приема</h1>
            <p>Понедельник - Четверг с 10:00 до 17:00</p>
            <p>Пятница с 10:00 до 16:00</p>
            <p>Перерыв с 12:00 до 13:00</p>
        </div>
    </main>

     <footer>  
        <?php include "footer.html"; ?>
    </footer>

    <div id="toTop">Наверх</div>

    <!-- Скрипты bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
    <!-- Pushy JS -->
    <script src="js/js_menu_pushy/pushy.min.js"></script>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Скрипты для кнопки "наверх" -->
    <script src="js/totopbutton.js"></script>
</body>

</html>